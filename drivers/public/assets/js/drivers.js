$(document).ready(function(){

  var searchData = new Object;

  $('#searchDrivers').bind('click', function(){

    // Made data object
    searchData.origem       = $('#origemInput').val();
    searchData.destino      = $('#destinoInput').val();
    searchData.veiculo      = $('#veiculoInput').val();
    searchData.carroceria   = $('#carroceriaInput').val();
    searchData.frete        = $('#freteInput').val();
    searchData.peso         = $('#pesoInput').val();

    // Insert datas on labels
    $('#labelOrigem').html(searchData.origem);
    $('#labelDestino').html(searchData.destino);
    $('#labelVeiculo').html(searchData.veiculo);
    $('#labelCarroceria').html(searchData.carroceria);
    $('#labelFrete').html("R$ " + searchData.frete + ",00");
    $('#labelPeso').html(searchData.peso + " kg");

    // Showing Pages
    $('.fase-1').css('display','none');
    $('.fase-2').css('display','block');

    // Muda tamanho Elementos
    $('.change-content').removeClass('col-xs-9').addClass('col-xs-8');
    $('.change-map').removeClass('col-xs-3').addClass('col-xs-4');
  });

  $('#editData').bind('click', function(){
    // Showing Pages
    $('.fase-2').css('display','none');
    $('.fase-1').css('display','block');

    // Muda tamanho Elementos
    $('.change-content').removeClass('col-xs-8').addClass('col-xs-9');
    $('.change-map').removeClass('col-xs-4').addClass('col-xs-3');
    
    $('.box-driver-detail').css('display','none');
    $('.box-map').css('display','block');
    $('.map-section').css('padding','0 !important');
  });

  $('.table-users tr').bind('click', function(){
    // Showing Pages
    $('.box-map').css('display','none');
    $('.box-driver-detail').css('display','block');
    $('.map-section').css('padding-left','15px !important');
  });

  $('.voltar-lista').bind('click', function(){
    // Showing Pages
    $('.box-driver-detail').css('display','none');
    $('.box-map').css('display','block');
    $('.map-section').css('padding','0 !important');
  });
});